﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        

    public Form1()
        {
            InitializeComponent();
            string path = @"Z:\Documents\basicmanaoff-master\taches.txt"; // Affiche les éléments du tableau à l'ouverture du fichier
            string[] lines = File.ReadAllLines(path);
            int cpt;
            this.dataGridView1.Rows.Clear();
            for (cpt = 0; cpt < (lines.Length - 3); cpt = cpt + 4)
            {
                this.dataGridView1.Rows.Add(lines[cpt], lines[cpt + 1], lines[cpt + 2],lines[cpt+ 3]);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnAjout_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "") 
            {
                MessageBox.Show("Une information est manquante !");
            }
            else
            {
                try
                {
                    string path = @"Z:\Documents\basicmanaoff-master\taches.txt";

                    List<string> listetache = new List();
                    listetache.Add(textBox1.Text);
                    listetache.Add(textBox2.Text);
                    listetache.Add(textBox3.Text);
                    listetache.Add(textBox4.Text);

                    //listetache.Add(dateTimePicker1.Value.Date); 


                    using (StreamWriter file = new StreamWriter(path, true))
                    {

                        file.WriteLine(listetache[0]); // Add this line at the end of the text file
                        file.WriteLine(listetache[1]);
                        file.WriteLine(listetache[2]);
                        file.WriteLine(listetache[3]);
                    } 
                    string textfichier = File.ReadAllText(path);

                   string[] lines = File.ReadAllLines(path);

                    this.dataGridView1.Rows.Clear();
                    int cpt2;
                    for (cpt2 = 0; cpt2 < (lines.Length - 3); cpt2 = cpt2 + 4)
                    {
                        this.dataGridView1.Rows.Add(lines[cpt2], lines[cpt2 + 1], lines[cpt2 + 2], lines[cpt2 + 3]);
                    }


                    this.textBox1.Text = "";
                    this.textBox2.Clear();
                    this.textBox3.Clear();
                    this.textBox4.Clear();


                }
            
                 catch (FormatException)
                {
                    MessageBox.Show("Test");
                }
            }
        }

        private void btnSupp_Click(object sender, EventArgs e) // Supprime une ligne du DGV * manque la fonctionqui supprime aussi sur le fichier*
        {
            try
            {
                DialogResult res = MessageBox.Show("Voulez-vous supprimer cette ligne?", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (res == DialogResult.OK)
                {
                    int index = this.dataGridView1.CurrentRow.Index;
                    this.dataGridView1.Rows.RemoveAt(index);
                    MessageBox.Show("Supprimer avec succès");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Aucune tâche sélectionnée");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.groupBox1.Visible = false;
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }
        int index;
        private void btnMod_Click(object sender, EventArgs e) // Bouton qui ouvre un formulaire de modifcation suite a un ID saisi
        {
            int trouve = 0;
           
            for (int i = 0; i < this.dataGridView1.Rows.Count -1; i++)
            {
                if (this.dataGridView1.Rows[i].Cells[0].Value.ToString()== txtID.Text)
                {
                    index = i;
                    trouve = 1;
                }
            }
            if (trouve == 0)
                MessageBox.Show("Tâche inexistante");
            else
                this.groupBox1.Visible = true;
        }

        private void button1_Click(object sender, EventArgs e) // Bouton appliquer qui permet de modifier les détails d'une tâche préalablement recherché
        {
            
                    this.dataGridView1.Rows[index].Cells[0].Value = NewID.Text;
                    this.dataGridView1.Rows[index].Cells[1].Value = NewName.Text;
                    this.dataGridView1.Rows[index].Cells[2].Value = NewDesc.Text;
                    this.dataGridView1.Rows[index].Cells[3].Value = textBox5.Text;
                    this.NewID.Clear();
                    this.NewName.Clear();
                    this.NewDesc.Clear();
                    this.textBox5.Clear();
                    this.groupBox1.Visible = false;
                    this.txtID.Clear();
                

        }

        private void NewID_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void btnRech_Click(object sender, EventArgs e) // Bouton qui permet de rechercher les détails suivant l'ID
        {
            int cmp = 0;
            for (int i = 0; i < this.dataGridView1.Rows.Count-1; i++)
            {
                if (this.dataGridView1.Rows[i].Cells[0].Value.ToString()==this.SearchID.Text) 
                {
                    MessageBox.Show("Nom : " + this.dataGridView1.Rows[i].Cells[1].Value + "\nDescription : " + this.dataGridView1.Rows[i].Cells[2].Value + "\nDate de fin: "+this.dataGridView1.Rows[i].Cells[3].Value);
                    cmp = 1;
                }
            }
            if (cmp == 0)
                MessageBox.Show("Tâche inexistante");
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }
        

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void btnOpen_Click(object sender, EventArgs e) // Bouton pour ouvrir un document *manque l'affichage*
        {
           OpenFileDialog tache = new OpenFileDialog();
            if (tache.ShowDialog() == DialogResult.OK)
            {
                dataGridView1.Rows.Add(tache);
            }
            /*{
                dataGridView1.Rows.Clear();
                string[] files = Directory.GetFiles(tache.SelectedPath);
                string[] dirs = Directory.GetDirectories(tache.SelectedPath);
                foreach (string file in files)
                {
                    dataGridView1.Rows.Add(file);
                }
            }*/
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged_1(object sender, EventArgs e)
        {

        }
    }
}
