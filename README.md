**BASIC TASK MANAGER**

Voici le projet C# de Terence et Billy

C'est une application windows form qui permet à un utlisateur de lister les tâches qu'il a à faire.
Il peut aussi rechercher une tâche, la modifier ou encore la supprimer.

Des améliorations sont à faire au niveau de la lecture et écriture du fichier.
Sur la console directement il est possible de modifier(Ajout/Modification/Suppression) le DGV mais nous n'avons pas réussi à modifier directement le fichier taches.txt.
Nous avons seulement réussi a écrire sur le fichier.

Quelques soucis de GIT notamment sur la fin en nous mettant au niveau de la solution et en faisant Git Bash here, des erreurs sont parvenues, notre dossier de base a été supprimé
et nous sommes reparti de la solution sur notre GIT d'où la présence des deux liens GIT.